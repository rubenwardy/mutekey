import pulsectl
from pynput.keyboard import Key, Listener

KEYCODE = "<65027>"

with pulsectl.Pulse("chatmuter") as pulse:
	def find_sink():
		sinks = pulse.sink_list()

		# Find "blue"tooth speaker
		return next(filter(lambda x: "blue" in x.name, sinks), None)


	needs_unmuting = False

	def key_changed(key, wasPressed):
		global needs_unmuting

		if str(key) != KEYCODE:
			return

		pulsectl.PulseVolumeInfo

		sink = find_sink()
		if sink is None:
			return

		if wasPressed:
			if not sink.mute:
				print("Muting")
				pulse.mute(sink, True)
				needs_unmuting = True
			else:
				print("Already muted")
				needs_unmuting = False

		elif needs_unmuting:
			print("Unmuting")
			pulse.mute(sink, False)


	with Listener(
			on_press = lambda key: key_changed(key, True),
			on_release = lambda key: key_changed(key, False)) as listener:
		listener.join()
